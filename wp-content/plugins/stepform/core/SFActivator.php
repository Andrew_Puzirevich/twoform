<?php

class SFActivator
{
    public static function activate() {

        require_once SF_PLUGIN_CORE_DIR.'Procedure.php';
        $procedure = new Procedure();
        $procedure->initSFTable();
    }
}