<?php
class Procedure
{
    public function SFDBConnect()
    {
        define('SF_TABLE', 'sf_formTable');
        $host = DB_HOST;
        $db   = DB_NAME;
        $user = DB_USER;
        $pass = DB_PASSWORD;
        $charset = DB_CHARSET;

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $pdo = new PDO($dsn, $user, $pass, $opt);

        return $pdo;
    }

    public function initSFTable()
    {
        $pdo = $this->SFDBConnect();

        try {
            $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $sql ="CREATE table SF_TABLE(
            ID INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
            user_name VARCHAR( 50 ) NOT NULL, 
            user_s_name VARCHAR( 50 ) NOT NULL,
            user_phone VARCHAR ( 20 ) NOT NULL, 
            user_mail VARCHAR( 150 ) NOT NULL, 
            user_adr VARCHAR( 150 ) NOT NULL, 
            user_zip INT( 100 ) NOT NULL,
            user_bill_adr VARCHAR( 250 ) NOT NULL,
            user_bill_state VARCHAR( 100 ) NOT NULL,
            user_card VARCHAR ( 25 ) NOT NULL,
            user_card_exp VARCHAR ( 5 ) NOT NULL ,
            user_cvv VARCHAR ( 3 ) NOT NULL );" ;
            $pdo->exec($sql);
            return true;

        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

    public function SFSetForm($name, $secondName, $phone, $mail, $adr, $zip)
    {
        $pdo = $this->SFDBConnect();
        if ($pdo->exec("INSERT INTO SF_TABLE (user_name, user_s_name, user_phone, user_mail, user_adr, user_zip) 
VALUES ('$name','$secondName','$phone','$mail','$adr','$zip')")){
            $lastID = $pdo->lastInsertId();
            return $lastID;
        } else{
            return false;
        }
    }

    public function SFUpdateForm($mail, $bilAdr, $bilSt, $cardNum, $cardExp, $cvv)
    {
        $pdo = $this->SFDBConnect();
        if ($pdo->query('UPDATE SF_TABLE SET user_bill_adr="'.$bilAdr.'", user_bill_state="'.$bilSt.'", user_card="'.$cardNum.'", user_card_exp="'.$cardExp.'", user_cvv="'.$cvv.'" WHERE user_mail="'.$mail.'"')){
            return true;
        } else return false;
    }
}