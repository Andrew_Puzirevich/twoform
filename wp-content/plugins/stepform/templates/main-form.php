<form id="msform">
    <fieldset>
        <h2 class="fs-title">Step 1</h2>
        <input required type="text" name="userName" id="userName" placeholder="Name" />
        <input required type="text" name="userSName" id="userSName" placeholder="Second Name" />
        <input required type="text" name="phone" id="phone" placeholder="(000) 000 00 00"/>
        <input required type="text" name="email" id="email" placeholder="Email" />
        <input required type="text" name="address" id="address" placeholder="Address"/>
        <input required type="number" name="zip" id="zip" placeholder="Zip code"/>
        <input required type="button" name="sf-next" class="sf-next action-button" value="Next" />
    </fieldset>
    <fieldset>
        <h2 class="fs-title">Step 2</h2>
        <input required type="text" name="billingAddress" id="billingAddress" placeholder="Billing Address" />
        <input required type="text" name="billingState" id="billingState" placeholder="Billing State" />
        <input required type="text" name="cardNumber" id="cardNumber" placeholder="Card Number" />
        <input required type="text" name="cardExpire" id="cardExpire" placeholder="Expire Date"/>
        <input required type="text" name="cvv" id="cvv" placeholder="CVV code"/>
        <input required type="submit" name="sf-submit" class="sf-submit action-button" value="Send" />
    </fieldset>
</form>