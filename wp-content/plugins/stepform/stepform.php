<?php
/*
Plugin Name: 2stepform
Description: Plugin with 2-step form
Author: Andrew Puzirevich
Version: 1.0.1
*/

define('SF_VERSION', '1.0.1');

define( 'SF_PLUGIN', __FILE__ );

define( 'SF_PLUGIN_DIR', untrailingslashit( dirname( SF_PLUGIN ) ) );

define( 'SF_PLUGIN_TEMP_DIR', SF_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR);

define( 'SF_PLUGIN_CSS_DIR', SF_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR);

define( 'SF_PLUGIN_CORE_DIR', SF_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR);



add_action( 'wp_enqueue_scripts', 'SFScriptsMethod', 11 );

/**
 * connect scripts & css
 */
function SFScriptsMethod() {
    wp_enqueue_script( 'jquery' );
    wp_enqueue_style('sf-style', plugins_url('/stepform/css/style.css'));
    wp_enqueue_script('masked',plugins_url('/stepform/js/masked.js'));
    wp_enqueue_script('sf-script', plugins_url('/stepform/js/sf.js'));

}

register_activation_hook( __FILE__, 'activateStepForm' );

/**
 * init plugin & create table in db when plugin activated
 * $res - result (true|error)
 */

function activateStepform() {
    require_once SF_PLUGIN_CORE_DIR.'SFActivator.php';

    SFActivator::activate();
}

add_shortcode('2stepform', 'getSFForm');
/**
 * @param $atts
 * @return false|string
 */
function getSFForm($atts)
{
    ob_start();


    require_once SF_PLUGIN_TEMP_DIR.'main-form.php';

    return ob_get_clean();
}
/**
 * func for showing rows from table in admin panel
 */
function showRows()
{
    echo 'here be rows from db';
}

add_action('wp_ajax_addFirstPart', 'addFirstPartSF');
add_action('wp_ajax_nopriv_addFirstPart', 'addFirstPartSF');
/**
 * save first part of form
 */
function addFirstPartSF() {
    require_once SF_PLUGIN_CORE_DIR.'Procedure.php';
    $procedure = new Procedure();

    $resID = $procedure->SFSetForm($_POST['name'],$_POST['secondName'],$_POST['phone'],$_POST['mail'], $_POST['adr'], $_POST['zip']);

    wp_die();
}

add_action('wp_ajax_addSecondPart', 'addSecondPartSF');
add_action('wp_ajax_nopriv_addSecondPart', 'addSecondPartSF');
/**
 * save second part of form
 */
function addSecondPartSF() {
    require_once SF_PLUGIN_CORE_DIR.'Procedure.php';
    $procedure = new Procedure();

    $procedure->SFUpdateForm($_POST['mail'], $_POST['bilAdr'],$_POST['bilSt'],$_POST['cardNum'],$_POST['cardExp'],$_POST['cvv']);

    wp_die();
}


function register2SFMenuPage()
{
    add_menu_page('2-step Form','2-step Form','read','2step-form-inserts', 'showRows');
}
add_action('admin_menu', 'register2SFMenuPage');